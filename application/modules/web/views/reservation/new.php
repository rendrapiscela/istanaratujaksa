<section class="content-header">
    <h1>
        Room Status Summary
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Transaksi</a></li>
        <li class="active">Room Status</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12 text-center">
            <?php foreach ($summaryStatus as $msr) { ?>
                <a class="btn btn-app btn-lg" style="background: <?php echo iconStatusRoom($msr['id'])['color']; ?>; color:white;" data-tooltip="tooltip" title="<?php echo $msr['description'] ?>">
                    <!-- Tooltip -->
                    <?php echo iconStatusRoom($msr['id'])['icon']; ?>
                    <span style="font-size : 3vh;" class="badge bg-purple"><?php echo $msr['jumlah'] ?></span>
                    <b style="font-size: 2.5vh;"><?php echo $msr['name']; ?></b>

                </a>
            <?php } ?>
        </div>
    </div>
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <?php foreach ($mapping as $key => $val) { ?>
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <div class="clearfix">
                            <div class="pull-left">
                                <h3 class="box-title text-bold"><?php echo $key . ' Floor'; ?></h3>
                            </div>
                            <div class="pull-right">

                            </div>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row" style="text-align: center;">
                            <?php $no = 1;
                            foreach ($val as $k => $v) {
                                //set status
                                $id = $v['room_id'];
                                $link = iconStatusRoom($v['status_id'], $id)['link'];
                            ?>
                                <a class="btn btn-app btn-lg button-room" style="background: <?php echo iconStatusRoom($v['status_id'])['color']; ?>; color:white;" data-tooltip="tooltip" title="<?php echo $v['status_kamar'] ?>" <?php echo $link;  ?> data-id="<?php echo $v['room_id']; ?>" data-status="<?php echo $v['status_id'] ?>">
                                    <!-- Tooltip -->
                                    <?php echo iconStatusRoom($v['status_id'])['icon']; ?>
                                    <b style="font-size: 2.5vh;"><?php echo $v['nomor_kamar']; ?></b>
                                </a>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div> <!-- end col -->
        <?php } ?>
    </div>
</section>

<div class="modal fade" id="modal-check-in">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Check In</h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Daftar Pelanggan</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Pelanggan Baru</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table id="table-customer" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Pelanggan</th>
                                        <th>No.Identitas</th>
                                        <th>Nomor Telepon</th>
                                        <th>Email</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <?php echo form_open("web/reservation/checkin_new_customer", ["class" => "form-horizontal", "enctype" => "multipart/form-data"]); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Jenis Identitas</label>
                                    <div class="col-sm-10">
                                        <!-- <input type="text" class="form-control" name="identity_type" id="identity_type" value="" required="required" > -->
                                        <select name="identity_type" id="identity_type" class="form-control" required>
                                            <option value="">--Pilih Jenis Identitas--</option>
                                            <?php
                                            $option = $this->Customer_model->optionJenisIdentitas();
                                            foreach ($option as $row) {
                                                echo "<option value='" . $row . "'>$row</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nomor Identitas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="identity_number" id="identity_number" value="" required="required" maxlength="16">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nama Pelanggan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" id="name" value="" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nomor Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" id="phone" value="" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" id="email" value="">
                                        <label class="label label-info">*Tidak wajib diisi</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Alamat Lengkap</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" id="address" rows="6"></textarea>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-check"></i>&nbsp;Lanjutkan</button>
                            </div><!-- /.box-footer -->
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->