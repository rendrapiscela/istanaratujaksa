<section class="content-header">
    <h1>
        Penginapan
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Transaksi</a></li>
        <li class="active">Penginapan</li>
    </ol>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3 class="box-title">Kamar Kosong</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-md">
                        <?php
                        $roomKosong = $this->Room_model->infoRooms('kosong');
                        foreach ($roomKosong as $row) {
                        ?>
                            <tr>
                                <td><strong><?php echo $row['nama']; ?></strong></td>
                                <td><span class="badge bg-green" style="font-size:11pt;"><?php echo $row['jumlah_kamar']; ?></span></td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3 class="box-title">Kamar Terisi</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-md">
                        <?php
                        $roomIsi = $this->Room_model->infoRooms('isi');
                        foreach ($roomIsi as $row) {
                        ?>
                            <tr>
                                <td><strong><?php echo $row['nama']; ?></strong></td>
                                <td><span class="badge bg-orange" style="font-size:11pt;"><?php echo $row['jumlah_kamar']; ?></span></td>
                                <td>
                                    <?php
                                    $namaKamar = explode(',', $row['nama_room']);
                                    foreach ($namaKamar as $kam) {
                                        echo '<span class="badge bg-red">' . $kam . '</span>&nbsp;';
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div> <!-- End Row -->
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3 class="box-title">Daftar Check In / Check Out</h3>
                            <select id='searchStatusRoom' name="filterCheck" class="select2 form-control box-shadow">
                                <option value='all'>--Semua Kamar--</option>
                                <option value='checkin' selected>Belum Checkout</option>
                                <option value='checkout'>Sudah Checkout</option>
                            </select>
                        </div>
                        <div class="pull-right">
                            <a href="javascript:void(0);" id="btn-check-in" class="btn btn-success btn-sm btn-create-data">
                                <i class="fa fa-calendar-check-o"></i>&nbsp;Check In
                            </a>
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>No Reservasi</th>
                                    <th>Tanggal Reservasi</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Durasi (Hari)</th>
                                    <th>Tanggal Akhir Reservasi</th>
                                    <th>Check In</th>
                                    <th>Check Out</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="modal-check-in">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Check In</h4>
            </div>
            <div class="modal-body">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_1" data-toggle="tab">Daftar Pelanggan</a></li>
                        <li><a href="#tab_2" data-toggle="tab">Pelanggan Baru</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_1">
                            <table id="table-customer" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Pelanggan</th>
                                        <th>No.Identitas</th>
                                        <th>Nomor Telepon</th>
                                        <th>Email</th>
                                        <th>Alamat</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="tab-pane" id="tab_2">
                            <?php echo form_open("web/reservation/checkin_new_customer", ["class" => "form-horizontal", "enctype" => "multipart/form-data"]); ?>
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Jenis Identitas</label>
                                    <div class="col-sm-10">
                                        <!-- <input type="text" class="form-control" name="identity_type" id="identity_type" value="" required="required" > -->
                                        <select name="identity_type" id="identity_type" class="form-control" required>
                                            <option value="">--Pilih Jenis Identitas--</option>
                                            <?php
                                            $option = $this->Customer_model->optionJenisIdentitas();
                                            foreach ($option as $row) {
                                                echo "<option value='" . $row . "'>$row</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nomor Identitas</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="identity_number" id="identity_number" value="" required="required" maxlength="16">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nama Pelanggan</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" id="name" value="" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Nomor Telepon</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" id="phone" value="" required="required">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" id="email" value="">
                                        <label class="label label-info">*Tidak wajib diisi</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Alamat Lengkap</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="address" id="address" rows="6"></textarea>
                                    </div>
                                </div>
                            </div><!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info pull-right"><i class="fa fa-check"></i>&nbsp;Lanjutkan</button>
                            </div><!-- /.box-footer -->
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->