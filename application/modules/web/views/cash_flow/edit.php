<style>
.clear{
 clear:both;
 margin-top: 20px;
}

#searchResult{
 list-style: none;
 padding: 0px;
 width: 250px;
 position: absolute;
 margin: 0;
 z-index: 9999;
}

#searchResult li{
 background: lavender;
 padding: 4px;
 margin-bottom: 1px;
 font-weight: bold;
}

#searchResult li:nth-child(even){
 background: lavender;
 color: white;
}

#searchResult li:hover{
 cursor: pointer;
}
</style>
<section class="content-header">
    <h1>
    Daftar Kas Masuk/Keluar
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="<?php echo base_url("web/cash_flow");?>">Daftar Kas Masuk/Keluar</a></li>
        <li class="active">Edit Data</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Pendapatan / Pengeluaran</h3>
                </div><!-- /.box-header -->
                <?php echo form_open("web/cash_flow/update", ["class"=>"form-horizontal", "enctype"=>"multipart/form-data"]); ?>
                    <?php echo form_hidden("id",$data->cash_flow_id); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Jenis Transaksi</label>
                            <div class="col-sm-10">
                                <?php echo form_dropdown('kode_akun_id', $akun, $data->cash_flow_kode_akun_id, 'class="select2"');?> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Keterangan</label>
                            <div class="col-sm-10">
                                <input type="text" value="<?php echo $data->cash_flow_keterangan; ?>" name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan Pendapatan/Pengeluaran">
                                <ul id="searchResult"></ul>
                                <span class="label text-blue">*Pilih jika keterangan sudah ada</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="cost" class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="jumlah" id="jumlah" value="<?php echo $data->cash_flow_jumlah; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost" class="col-sm-2 control-label">Total Harga</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" name="harga" id="harga" value="<?php echo $data->cash_flow_harga; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Foto</label>
                            <div class="col-sm-10">
                                <input type="file" name="file" class="file-input-image" />
                                <p></p>
                                <?php echo imageExist($data->cash_flow_image); ?>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i>&nbsp;Update</button>
                    </div><!-- /.box-footer -->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>