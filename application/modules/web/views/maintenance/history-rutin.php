<div class="col-md-12">
        <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">History Pemeriksaan Kamar <?php echo $room->rooms_number.' - Status Kamar <span class="label label-primary">'.$room->status_room_name.'</span>'; ?></h3>
                </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(empty($history)){ ?>
                            <div class="alert alert-info">
                                <i class="fa fa-info"></i> Belum ada data pemeriksaan kamar.
                            </div>
                        <?php }else{ ?>
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th>Status Kamar</th>
                                        <th>Tanggal Cek</th>
                                        <th>Oleh</th>
                                        <th style="text-align: center;">History</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no = 1;
                                    foreach($history as $row){
                                    ?>
                                    <tr>
                                        <td><?php echo $row->status_name; ?></td>
                                        <td><?php echo $row->created_at; ?></td>
                                        <td><?php echo $row->user_name; ?></td>
                                        <td style="text-align: center;">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal-default-<?php echo $row->id; ?>">
                                            <i class="fa fa-eye"></i> Lihat History
                                        </button>
                                        <div class="modal fade" id="modal-default-<?php echo $row->id; ?>">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title"><?php echo convert_date($row->created_at, true) ?></h4>
                                                    </div>
                                                        <div class="modal-body">
                                                            <table class="table table-striped table-bordered">
                                                                <thead>
                                                                    <tr class="bg-orange">
                                                                        <th style="text-align: center;">Ceklist Kamar</th>
                                                                        <th style="text-align: center;">Kondisi</th>
                                                                        <th style="text-align: center;">Detail</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach($row->detail as $detail){ ?>
                                                                    <tr>
                                                                        <td style="text-align: left;"><?php echo $detail->nama_check ?></td>
                                                                        <td><?php echo ($detail->kondisi == 'Baik') ? "<label class='label label-success'><i class='fa fa-check'></i> ".$detail->kondisi."</label>" : "<label class='label label-danger'><i class='fa fa-times'></i> ".$detail->kondisi."</label>" ?></td>
                                                                        <td style="text-align: left;"><?php echo $detail->keterangan ?></td>
                                                                    </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                            </div>
                        <?php }//endif ?>
                    </div><!-- /.box-body -->
            </div>

        </div> <!-- End History -->
