<div class="col-md-12">
        <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">History Pemeliharaan Kamar <?php echo $room->rooms_number.' - Status Kamar <span class="label label-primary">'.$room->status_room_name.'</span>'; ?></h3>
                </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php if(empty($history)){ ?>
                            <div class="alert alert-info">
                                <i class="fa fa-info"></i> Belum ada data pemeliharaan kamar.
                            </div>
                        <?php }else{ ?>
                            <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="table">
                                <thead>
                                    <tr>
                                        <th style="width:5%;">No</th>
                                        <th>Tanggal Cek</th>
                                        <th>Jenis Asset / Barang</th>
                                        <th>Kondisi Detail Masalah</th>
                                        <th>Status Kondisi</th>
                                        <th>Oleh</th>
                                        <th>Solusi</th>
                                        <th>Budget</th>
                                        <th>Tindak Lanjut</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $no = 1;
                                    foreach($history as $row){
                                    ?>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $row->tanggal_check; ?></td>
                                        <td><?php echo $row->activity_name; ?></td>
                                        <td><?php echo $row->deskripsi; ?></td>
                                        <td><?php echo $row->status_activity; ?></td>
                                        <td><?php echo $row->user_name; ?></td>
                                        <td><?php echo $row->solution; ?></td>
                                        <td><?php echo (!is_null($row->budget) ? number_format($row->budget,0,'.','.') : ''); ?></td>
                                        <td>
                                            <?php if($row->is_tl == 'sudah-tl'){ ?>
                                                <a href="<?php echo base_url('web/maintenance/tindak_lanjut/'.$room->rooms_id.'/'.$row->id); ?>"
                                                ><i class="fa fa-check"></i> <?php echo status_tindak_lanjut($row->is_tl); ?></a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url('web/maintenance/tindak_lanjut/'.$room->rooms_id.'/'.$row->id); ?>"
                                                ><?php echo status_tindak_lanjut($row->is_tl); ?></a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php $no++; } ?>
                                </tbody>
                            </table>
                            </div>
                        <?php }//endif ?>
                    </div><!-- /.box-body -->
            </div>

        </div> <!-- End History -->
