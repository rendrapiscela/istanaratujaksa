<section class="content-header">
    <h1>
         Pemeliharaan Kamar
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active"> Pemeliharaan Kamar</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="clearfix">
                        <div class="pull-left">
                            <h3 class="box-title">Daftar Pemeliharaan Kamar</h3>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                    <table id="table" class="table table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Kategori</th>
                                <th>Nomor Kamar</th>
                                <th style="text-align:center;">Budget Pemeliharan</th>
                                <th style="text-align:center;">Status Kamar</th>
                                <th style="text-align:center;">Terakhir Cek</th>
                                <th style="text-align:center;">Pemeliharaan</th>
                                <th style="text-align:center;">Ceklist</th>
                            </tr>
                        </thead>
                        <?php if(!empty($data)){ ?>
                        <tbody>
                            <?php foreach($data as $idx => $rows){ ?>
                            <tr>
                                <td><?php echo ($idx+1); ?></td>
                                <td><?php echo $rows->categories_room_name; ?></td>
                                <td><?php echo $rows->number; ?></td>
                                <td align="center"><?php echo format_rupiah($rows->budget_trouble); ?></td>
                                <td align="center"><?php echo $rows->status; ?></td>
                                <td align="center"><?php echo $rows->tanggal_cek ?? "<label class='label label-danger'><i class='fa fa-times'></i> Belum Dicek</label>"; ?></td>
                                <td style="width:10%;">
                                    <a href="<?php echo base_url("web/maintenance/pemeliharaan/".$rows->id); ?>" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Input Kerusakan</a>
                                </td>
                                <td>
                                    <a href="<?php echo base_url("web/maintenance/cek_rutin/".$rows->id); ?>" class="btn btn-xs btn-success"><i class="fa fa-list"></i> Cek Rutin</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                        <?php } ?>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>