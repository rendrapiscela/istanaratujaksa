<section class="content-header">
    <h1>
         Kamar <?php echo $room->categories_room_name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="<?php echo base_url("web/maintenance");?>">Pemeliharaan Kamar</a></li>
        <li class="active"><?php echo $room->categories_room_name." - ".$room->rooms_number; ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-6">
            <div class="box box-primary box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Pemeliharaan Kamar <?php echo $room->rooms_number; ?></h3>
                </div><!-- /.box-header -->
                <?php echo form_open("web/maintenance/store_pemeliharaan/".$room->rooms_id, ["class"=>"form-horizontal", "enctype"=>"multipart/form-data"]); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Tanggal Cek</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="tanggal_check" value="<?php echo now(); ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Jenis Asset / Barang </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="activity_name" value="" rrequired>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Kondisi Detail Masalah</label>
                            <div class="col-sm-8">
                                <textarea name="deskripsi" class="form-control" rows="3" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Status Kondisi</label>
                            <div class="col-sm-8">
                                <select class="select2 form-control" name="status_activity" required>
                                    <option value="">-- Pilih Status Kondisi --</option>
                                    <?php 
                                    if(!empty($statusActivity)){
                                        foreach(json_decode($statusActivity,1) as $row){
                                            echo "<option value='".$row."'>".$row."</option>";    
                                        }                                        
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-sm">Simpan</button>
                    <a href="<?php echo base_url("web/maintenance");?>" class="btn btn-default btn-sm">Batal</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div> <!-- End Pemeliharaan -->
        
        <?php $this->load->view('maintenance/history') ?>
        <!-- End History -->

    </div>
</section>