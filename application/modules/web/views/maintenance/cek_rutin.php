<section class="content-header">
    <h1>
         Kamar <?php echo $room->categories_room_name; ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="<?php echo base_url("web/maintenance");?>">Pemeriksaan Rutin Kamar</a></li>
        <li class="active"><?php echo $room->categories_room_name." - ".$room->rooms_number; ?></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-10">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form Ceklist Rutin Kamar <?php echo $room->rooms_number; ?></h3>
                </div><!-- /.box-header -->
                <?php echo form_open("web/maintenance/store_rutin/".$room->rooms_id, ["class"=>"form-horizontal", "enctype"=>"multipart/form-data"]); ?>
                    <div class="box-body">
                        <div class="form-group" style="display: none;">
                            <label for="" class="col-sm-3 control-label">Tanggal Cek</label>
                            <div class="col-sm-4">
                                <input type="text" class="form-control" name="tanggal_check" value="<?php echo now(); ?>" readonly>
                            </div>
                        </div>
                        <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr class="bg-orange">
                                    <th style="width:35%;">Ceklist Keadaan Kamar</th>
                                    <?php 
                                    $rutin = json_decode($statusRutin,1);
                                    foreach($rutin as $row){
                                        echo "<th style='text-align:center; width:10%;'>".$row."</th>";
                                    }
                                    ?>
                                    <th style="text-align: center;">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($fasilitas as $fas) {?>
                                <tr>
                                    <td><b><?php echo $fas->jenis_check_nama_check; ?></b>
                                        <input type="hidden" name="jenis_check_id[]" value="<?php echo $fas->jenis_check_id; ?>">
                                    </td>
                                    <?php 
                                    $rutin = json_decode($statusRutin,1);
                                    foreach($rutin as $row){
                                        echo "<td style='text-align:center;'>";
                                        echo "<input type='radio' name='status_check_id_".$fas->jenis_check_id."' value='".$row."' class='minimal' required>";
                                        echo "</td>";
                                    }
                                    ?>
                                    <td style="text-align:center;">
                                        <textarea name="keterangan[]" class="form-control"></textarea>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>

                        <div class="bg-red" style="padding:1%; margin-top: 2em;">
                        <div class="form-group">
                            <label for="" class="col-sm-4 control-label">Ubah Status Kamar</label>
                            <div class="col-sm-8">
                                <select class="select2 form-control" name="status_kamar" required>
                                    <option value="">-- Pilih Status Kamar --</option>
                                    <?php 
                                    if(!empty($statusRuangan)){
                                        foreach($statusRuangan as $row){
                                            $selected = ($room->status_room_id == $row->status_room_id) ? "selected" : "";
                                            echo "<option value='".$row->status_room_id."' $selected>".$row->status_room_name."</option>";    
                                        }                                        
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        </div>

                    </div><!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btn-md">Simpan Kondisi Kamar</button>
                    <a href="<?php echo base_url("web/maintenance");?>" class="btn btn-default btn-md">Batal</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div> <!-- End Pemeliharaan -->
        
        <?php $this->load->view('maintenance/history-rutin') ?>
        <!-- End History -->

    </div>
</section>