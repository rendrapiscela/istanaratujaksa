<section class="content-header">
    <h1>
         Fasilitas
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="<?php echo base_url("web/room");?>">Fasilitas</a></li>
        <li class="active">Tambah Baru</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form  Fasilitas Pengecekkan Kamar</h3>
                </div><!-- /.box-header -->
                <?php echo form_open("web/fasilitas/store", ["class"=>"form-horizontal"]); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Nama Fasilitas</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama_check" id="nama_check" value="" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Merk</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="merk" id="merk" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Keterangan Lain</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="6"></textarea>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    </div><!-- /.box-footer -->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>