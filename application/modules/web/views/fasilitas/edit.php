<section class="content-header">
    <h1>
         Fasilitas
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Master Data</a></li>
        <li><a href="<?php echo base_url("web/room");?>">Fasilitas</a></li>
        <li class="active">Edit Data</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?php $this->load->view("layouts/alert"); ?>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Form  Fasilitas Ruangan</h3>
                </div><!-- /.box-header -->
                <?php echo form_open("web/fasilitas/update", ["class"=>"form-horizontal", "enctype"=>"multipart/form-data"]); ?>
                    <?php echo form_hidden("id",$data->jenis_check_id); ?>
                    <div class="box-body">
                        <div class="form-group">
                            <label for="" class="col-sm-2 control-label">Nama Fasilitas</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="nama_check" id="nama_check" value="<?php echo $data->jenis_check_nama_check; ?>" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Merk</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="merk" id="merk" rows="3"><?php echo $data->jenis_check_merk; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Keterangan Lain</label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="keterangan" id="keterangan" rows="6"><?php echo $data->jenis_check_keterangan; ?></textarea>
                            </div>
                        </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info pull-right"><i class="fa fa-save"></i>&nbsp;Simpan</button>
                    </div><!-- /.box-footer -->
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</section>