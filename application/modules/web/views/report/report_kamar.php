<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo APP_NAME.' | Laporan'; ?></title>

    <style>
        .invoice-box {
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
        <div class="logo" style="position: absolute;">
            <img src="<?php echo base_url('assets/dist/img/background.jpg');?>" style="width:100%; max-width:120px;">
        </div>
         <div style="text-align:center;">
            <h3>Laporan Pemakaian Kamar</h3>
            <h4><?php echo $first." s/d ".$last; ?></h4>
         </div>
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped" style="font-size : 10pt; width:30%; border-collapse: collapse;" border="1">
            <tr class="heading">
                <td>Tansaksi</td>
                <td>Jumlah</td>
                <td>Income</td>
            </tr>
            <?php
            $totali = 0;
            $totalTransaksi = 0;
            foreach($sumJenisTransaksi as $rw){
                $totali += $rw->total_income;
                $totalTransaksi += $rw->jumlah_transaksi;
                echo "<tr>";
                echo "<td>".$rw->jenis_transaksi."</td>";
                echo "<td>".format_rupiah($rw->jumlah_transaksi)."</td>";
                echo "<td>".format_rupiah($rw->total_income)."</td>";
                echo "</tr>";
            }
                echo "<tr>";
                echo "<td><b>Total</b></td>";
                echo "<td style='font-weight:bold;'>".format_rupiah($totalTransaksi)."</td>";
                echo "<td style='font-weight:bold;'>".format_rupiah($totali)."</td>";
                echo "</tr>";
            ?>
        </table>
        <table cellpadding="0" cellspacing="0" class="table table-bordered" style="font-size:10pt;">
            
            <tr class="heading">
                <td rowspan="2" style="vertical-align:middle; text-align:center;">Tanggal</td>
                <td colspan="3" style="text-align:center;">Room</td>
                <td rowspan="2" style="vertical-align:middle; text-align:center;">Income</td>
            </tr>
            <tr class="heading" style="text-align:center;">
                <td>Standar</td>
                <td>Superior</td>
                <td>Deluxe</td>
            </tr>
           
            <?php if(count($data) > 0): ?>
                <?php
                    $income = 0; 
                    $total_1 = 0; $total_2=0; $total_3=0;
                    foreach($data as $row): 
                        $income += $row->income;
                        $total_1 += $row->jumlah_1;
                        $total_2 += $row->jumlah_2;
                        $total_3 += $row->jumlah_3;
                    ?>
                    <?php if(!is_null($row->invoice_date)): ?>
                        <tr class="item" style="text-align: center;">
                            <td><?php echo $row->invoice_date;?></td>
                            <td><?php echo $row->jumlah_1;?></td>
                            <td><?php echo $row->jumlah_3;?></td>
                            <td><?php echo $row->jumlah_2;?></td>
                            <td style="text-align: right !important; font-weight:bold;"><?php echo "Rp. ".format_rupiah($row->income);?></td>
                        </tr>
                    <?php Else: ?>
                        <tr class="total">
                            <td colspan="1"><strong>TOTAL</strong></td>
                            <td><strong><?php echo $total_1; ?></strong></td>
                            <td><strong><?php echo $total_3; ?></strong></td>
                            <td><strong><?php echo $total_2; ?></strong></td>
                            <td><?php echo format_rupiah($income);?></td>
                        </tr> 
                    <?php EndIf; ?>   
                <?php EndForeach; ?>
                        <tr class="total">
                            <td style="text-align: left !important; font-weight:bold;"><strong>TOTAL</strong></td>
                            <td style="text-align: center !important; font-weight:bold;"><strong><?php echo $total_1; ?></strong></td>
                            <td style="text-align: center !important; font-weight:bold;"><strong><?php echo $total_3; ?></strong></td>
                            <td style="text-align: center !important; font-weight:bold;"><strong><?php echo $total_2; ?></strong></td>
                            <td style="text-align: right !important; font-weight:bold;"><?php echo "Rp. ".format_rupiah($income);?></td>
                        </tr> 
            <?php Else: ?>
                <tr><td colspan='5' style="text-align:center;">-- Tidak Ada Data --</td></tr>
            <?php EndIf; ?>

           
            
        </table>
        </div>
    </div>
</body>

</html>