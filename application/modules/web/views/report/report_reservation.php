<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title><?php echo APP_NAME.' | Laporan'; ?></title>

    <style>
        .invoice-box {
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        
        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.top table td.title {
            font-size: 45px;
            line-height: 45px;
            color: #333;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            border-bottom: 1px solid #ddd;
            font-weight: bold;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.item td {
            border-bottom: 1px solid #eee;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.total td:nth-child(2) {
            border-top: 2px solid #eee;
            font-weight: bold;
        }

        @media only screen and (max-width: 600px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }

        /** RTL **/
        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
        body{
            zoom : 75%;
        }
        .align-middle {
            vertical-align: middle; 
            text-align:center;
        }
        .text-center{
            text-align: center !important;
        }
    </style>
</head>

<body>
    <div class="invoice-box">
    <div class="logo" style="position: absolute;">
            <img src="<?php echo base_url('assets/dist/img/background.jpg');?>" style="width:100%; max-width:120px;">
    </div>
         <div style="text-align:center;">
            <h3>Laporan Transaksi Penginapan</h3>
            <h4><?php echo $first." s/d ".$last; ?></h4>
         </div>
        <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" class="table table-striped table-condensed" style="font-size:10pt;">
            
            <tr class="heading">
                <td rowspan="2" class="align-middle">Trans</td>
                <td rowspan="2" class="align-middle">No Invoice</td>
                <td rowspan="2" class="align-middle">Nama Pelanggan</td>
                <td rowspan="2" class="align-middle">Tgl Invoice</td>
                <td colspan="2" class="align-middle">Tgl Check In</td>
                <td colspan="2" class="align-middle">Tgl Check Out</td>
                <td rowspan="2" class="align-middle">Kategori Kamar</td>
                <td rowspan="2" class="align-middle">No Kamar</td>
                <td rowspan="2" class="align-middle">Harga / Hari</td>
                <td rowspan="2" class="align-middle">Durasi / Hari</td>
                <td rowspan="2" class="align-middle">Total</td>
                <td rowspan="2" class="align-middle">Deposit</td>
                <td rowspan="2" class="align-middle">Total Diskon</td>
                <td rowspan="2" class="align-middle">Grand Total</td>
                <td rowspan="2" class="align-middle">Piutang (Cash)</td>
                <td rowspan="2" class="align-middle">Hutang (Non-Cash)</td>
            </tr>
            <tr>
                <td class="align-middle text-center">Tgl</td>
                <td class="align-middle text-center">Jam</td>
                <td class="align-middle text-center">Tgl</td>
                <td class="align-middle text-center">Jam</td>
            </tr>
           
            <?php if(count($data) > 0): ?>
                <?php 
                $totalDue = 0;
                $totalPiutang = 0; $totalHutang = 0;
                $totalDeposit = 0;
                    foreach($data as $row): 
                        $totalDue += $row->due;                        
                        $checkIn = explode(" ", $row->date_checkin);
                        $checkOut = "";
                        if(!empty($row->date_checkout)){
                            $checkOut = explode(" ", $row->date_checkout);
                        }else{
                            $totalDeposit += $row->deposit;
                        }

                ?>
                    <?php if(!is_null($row->invoice_number)): ?>
                        <tr class="item">
                            <td><?php echo $row->jenis_transaksi;?></td>
                            <td><?php echo $row->invoice_number;?></td>
                            <td><?php echo $row->customer_name;?></td>
                            <td><?php echo $row->invoice_date;?></td>
                            <td><?php echo $checkIn[0]; ?></td>
                            <td><?php echo $checkIn[1]; ?></td>
                            <td><?php echo is_array($checkOut) ? $checkOut[0] : '-'; ?></td>
                            <td><?php echo is_array($checkOut) ? $checkOut[1] : '-'; ?></td>
                            <td><?php echo $row->category_name; ?></td>
                            <td><?php echo $row->room_number; ?></td>
                            <td><?php echo number_format($row->price,0,'.','.');?></td>
                            <td style="text-align: center;"><?php echo $row->duration;?></td>
                            <td style="text-align: center;"><?php echo format_rupiah($row->total);?></td>
                            <td style="text-align: center;"><?php echo !is_array($checkOut) ? format_rupiah($row->deposit) : 0;?></td>
                            <td style="text-align: center;"><?php echo format_rupiah($row->discount);?></td>
                            <td><b><?php echo format_rupiah($row->due);?></b></td>
                            <td>
                                <b>
                                <?php 
                                    if($row->jenis_transaksi == "Walk In"){
                                        $totalPiutang += $row->due;
                                        echo format_rupiah($row->due);
                                    }
                                ?>
                                </b>
                            </td>
                            <td>
                                <b>
                                <?php 
                                    if($row->jenis_transaksi == "Traveloka"){
                                        $totalHutang += $row->due;
                                        echo format_rupiah($row->due);
                                    }
                                ?>
                                </b>
                            </td>
                        </tr>
                    <?php Else: ?>
                        <tr class="total">
                            <td colspan="16"><strong>TOTAL</strong></td>
                            <td style="text-align:right;"><?php echo format_rupiah($row->due);?></td>
                        </tr> 
                    <?php EndIf; ?>   
                <?php EndForeach; ?>
                <tr class="total">
                    <td colspan="13"><strong>TOTAL</strong></td>
                    <td class="text-center" style="text-align:center;"><b><?php echo format_rupiah($totalDeposit);?></b></td>
                    <td></td>
                    <td><?php echo format_rupiah($totalDue);?></td>
                    <td><b><?php echo format_rupiah($totalPiutang);?></b></td>
                    <td><b><?php echo format_rupiah($totalHutang);?></b></td>
                </tr>
            <?php Else: ?>
                <tr><td colspan='13' style="text-align:center;">-- Tidak Ada Data --</td></tr>
            <?php EndIf; ?>

           
            
        </table>
        </div>
    </div>
</body>

</html>