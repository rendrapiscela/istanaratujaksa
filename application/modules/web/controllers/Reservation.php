<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reservation extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->route = "web/reservation";
        $this->model = "Invoice_model";
        $this->template->title = "Penginapan";
        $this->template->javascript->add(site_url('assets/app/js/reservation.js?=' . date('ymdhis')));
        $this->load->model("Category_room_model", "categories");
        $this->load->model("Extra_model", "extra");
        $this->load->model("Service_model", "service");
        $this->load->model('Customer_model');
        $this->load->model('Room_model');
        $this->load->model('Status_room_model');
    }

    public function new()
    {
        //unset session
        $this->session->unset_userdata('idRoom');
        $this->session->unset_userdata('categoryIdRoom');
        $this->session->unset_userdata('numberRoom');

        $roleName = roles(); //role string strict
        $data['statusRoom'] = $this->Room_model->statusRoom();
        //get only column lantai kamar from array
        $data['lantai'] = array_column($data['statusRoom'], 'lantai_kamar');
        $floor = array_unique($data['lantai']);
        //get where lantai_kamar = floor
        $mapping = array();
        foreach ($floor as $fl) {
            foreach ($data['statusRoom'] as $key => $value) {
                if ($value['lantai_kamar'] == $fl) {
                    $mapping[$fl][] = $value;
                }
            }
        }
        // echo "<pre>";
        // print_r($mapping);
        // die();
        $data['mapping'] = $mapping;
        $data['summaryStatus'] = $this->Status_room_model->summary();
        $data['roleName'] = $roleName;
        $data['masterStatusRoom'] = $this->db->from('status_room')->get()->result_array();
        $this->template->content->view($this->route . '/new', $data);
        $this->template->javascript->add(site_url('assets/app/js/room_status_summary.js?=' . date('ymdhis')));
        $this->template->publish();
    }
    public function get_post()
    { //data table
        $this->load->model($this->model, "mdl");
        $draw = $this->input->post('draw');
        $row = $this->input->post('start');
        $rowperpage = 10; // Rows display per page
        $columnIndex = $this->input->post('order')[0]['column']; // Column index
        $columnName = $this->input->post('columns')[$columnIndex]['data']; // Column name
        $columnSortOrder = $this->input->post('order')[0]['dir']; // asc or desc
        $searchValue = $this->input->post('search')['value']; // Search value

        ## Custom Field value
        $filterStatusRoom = $this->input->post('filterStatusRoom');

        ## Search 
        $searchQuery = " ";
        if ($filterStatusRoom != 'all') {
            if ($filterStatusRoom == 'checkout') {
                $searchQuery .= " and (invoices.check_out_on is not null) ";
            } else if ($filterStatusRoom == 'checkin') {
                $searchQuery .= " and (invoices.check_out_on is null) ";
            }
        }
        if ($searchValue != '') {
            $searchQuery .= " and (customers.name like '%" . $searchValue . "%' or 
            invoices.invoice_number like '%" . $searchValue . "%' or 
            invoices.jenis_transaksi like'%" . $searchValue . "%' ) ";
        }

        $response = $this->mdl->formatDataTable($draw, $searchQuery, $columnName, $columnSortOrder, $row, $rowperpage);
        echo $response;
    }

    public function set_session_room(){
        $idRoom = $this->input->post('idRoom');
        $row = $this->Room_model->rowRoom($idRoom);
        if(!empty($row)){
            $this->session->set_userdata('idRoom', $row->id);
            $this->session->set_userdata('categoryIdRoom', $row->category_id);
            $this->session->set_userdata('numberRoom', $row->number);
        }
    }

    public function create()
    {
        checkPermission($this->route, "create");
        $this->load->model($this->model, "mdl");
        $id = $this->mdl->createInvoice(0);
        redirect($this->route . '/edit/' . $id);
    }

    public function checkin_customer($id)
    {
        checkPermission($this->route, "create");
        $this->load->model($this->model, "mdl");
        $invoice_id = $this->mdl->createInvoice(0, $id);
        redirect($this->route . '/edit/' . $invoice_id);
    }

    public function checkin_new_customer()
    {
        $_POST = array_map('strip_tags', $_POST);
        checkPermission($this->route, "create");
        $this->load->model($this->model, "mdl");
        $this->load->model("Customer_model", "customer");
        $post = $this->input->post(NULL, TRUE);
        $customer_id = $this->customer->store($post);
        $invoice_id = $this->mdl->createInvoice(0, $customer_id);
        redirect($this->route . '/edit/' . $invoice_id);
    }

    public function edit($id)
    {
        checkPermission($this->route, "update");
        $this->load->model($this->model, "mdl");
        $data = $this->mdl->find($id);
        $items = [
            "data" => $data,
            "categories" => $this->categories->getAll(),
            "jenis_transaksi" => jenis_transaksi(),
            "extra" => $this->extra->getAll(),
            "service" => $this->service->getAll(),
            "detail_rooms" => $this->mdl->getDetailRoom($id),
            "detail_taxes" => $this->mdl->getInvoiceTax($id, 0),
            "detail_discounts" => $this->mdl->getInvoiceDiscount($id, 0),
            "serviceSelected" => $this->mdl->serviceSelected($id, true),
            "extraSelected" => $this->mdl->extraSelected($id, true)
        ];
        if (is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak');
        $this->template->content->view($this->route . "/edit", $items);
        $this->template->publish();
        //unset session
        $this->session->unset_userdata('idRoom');
        $this->session->unset_userdata('categoryIdRoom');
        $this->session->unset_userdata('numberRoom');
    }

    public function show($id)
    {
        checkPermission($this->route, "view");
        $this->load->model($this->model, "mdl");
        $data = $this->mdl->find($id);
        $items = [
            "data" => $data,
            "categories" => $this->categories->getAll(),
            "detail_rooms" => $this->mdl->getDetailRoom($id),
            "detail_taxes" => $this->mdl->getInvoiceTax($id, 0),
            "detail_discounts" => $this->mdl->getInvoiceDiscount($id, 0),
            "serviceSelected" => $this->mdl->serviceSelected($id, false),
            "extraSelected" => $this->mdl->extraSelected($id, false),
            "links" => [
                "back" => base_url($this->route),
                "edit" => base_url($this->route . "/edit/" . $id),
                "delete" => base_url($this->route . "/delete/" . $id),
            ]
        ];
        if (is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak');
        $this->template->content->view($this->route . "/show", $items);
        $this->template->publish();
    }

    public function update()
    {
        checkPermission($this->route, "update");
        $id = $this->input->post('id');
        $this->load->model($this->model, "mdl");
        $post = $this->input->post(NULL, TRUE);
        $this->mdl->updateReservation($post, $id);
        $this->session->set_flashdata('success', 'Data reservasi berhasil diperbaharui');
        redirect($this->route . '/show/' . $id);
    }

    public function invoice($id)
    {
        checkPermission($this->route, "view");
        $this->load->model($this->model, "mdl");
        $data = $this->mdl->find($id);
        $items = [
            "data" => $data,
            "categories" => $this->categories->getAll(),
            "detail_rooms" => $this->mdl->getDetailRoom($id),
            "detail_taxes" => $this->mdl->getInvoiceTax($id, 0),
            "detail_discounts" => $this->mdl->getInvoiceDiscount($id, 0),
            "serviceSelected" => $this->mdl->serviceSelected($id, false),
            "extraSelected" => $this->mdl->extraSelected($id, false)
        ];
        if (is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak');
        $this->load->view("reservation/invoice", $items);
    }

    public function invoice_barcode($code)
    {
        checkPermission($this->route, "view");
        $this->load->library('Zend');
        $this->zend->load('Zend/Barcode');
        Zend_Barcode::render('code128', 'image', array('text' => $code), array());
    }
}
