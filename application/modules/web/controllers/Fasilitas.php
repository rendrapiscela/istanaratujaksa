<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Fasilitas extends MY_Controller{

    public function __construct() {
        parent::__construct();
        $this->cleanInput = true;
        $this->route = "web/fasilitas";
        $this->model = "Fasilitas_model";
        $this->template->title = "Fasilitas Ruangan";
        $this->template->javascript->add(site_url('assets/app/js/fasilitas.js?v=' . time()));
        $this->load->model("Fasilitas_model", "fasilitas");
        $this->load->model("Status_room_model", "status");
		$this->load->model("Category_room_model", "categories");
    }

    public function create(){
		checkPermission($this->route,"create");
		$items =[
            "status"=>$this->status->toSelectItem("name", "id"),
			"categories"=>$this->categories->toSelectItem("name", "id"),
        ];
		$this->template->content->view($this->route.'/create', $items);
        $this->template->publish();
    }

    public function show($id){
		checkPermission($this->route,"view");
		$data = $this->fasilitas->getRow($id);
		$items = [
			"data"=>$data,
			"links"=>[
				"back"=>base_url($this->route),
				"create"=>base_url($this->route."/create"),
				"edit"=>base_url($this->route."/edit/".$id),
				"delete"=>base_url($this->route."/delete/".$id),
			]
		];
		if(is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak'); 
		$this->template->content->view($this->route."/show",$items);
        $this->template->publish();
    }
    
    public function edit($id){
		checkPermission($this->route,"update");
		$this->load->model($this->model,"mdl");
		$data = $this->mdl->find($id);
        $items =[
            "data"=>$data
        ];
		if(is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak'); 
		$this->template->content->view($this->route."/edit",$items);
        $this->template->publish();
	}

    // protected function beforeCreate(array $data){ 
    //     $data["image"] = null;
    //     $upload = uploadFile();
    //     if(!is_null($upload)) $data["image"] = $upload;
    //     return $data; 
    // }

    // protected function beforeUpdate(array $data, $oldData, $id){ 
    //     if(!is_null($oldData->categories_room_image)){
    //         if(file_exists($oldData->categories_room_image)){
    //             unlink($oldData->categories_room_image);
    //         }
    //     }
    //     $data["image"] = null;
    //     $upload = uploadFile();
    //     if(!is_null($upload)) $data["image"] = $upload;
    //     return $data;
    // }


}