<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cash_flow extends MY_Controller{

    public function __construct() {
        parent::__construct();
        $this->cleanInput = true;
        $this->route = "web/cash_flow";
        $this->model = "Cash_model";
        $this->template->title = "Kas Masuk/Keluar";
        $this->template->javascript->add(site_url('assets/app/js/cash-flow.js'));
        $this->load->model("Status_room_model", "status");
		$this->load->model("Category_room_model", "categories");
        $this->load->model("Kode_akun_model", "kode_akun");
    }

    public function create(){
		checkPermission($this->route,"create");
		$items =[
            "akun" => $this->kode_akun->toSelectItem("akun", "id"),
        ];
		$this->template->content->view($this->route.'/create', $items);
        $this->template->publish();
    }

    public function store(){
		try{
            checkPermission($this->route,"create");
            $this->load->model($this->model, "mdl");
			$this->mdl->createValidation($this->form_validation);
			if ($this->form_validation->run() == TRUE) {
                $kodeAkun = $this->input->post('kode_akun_id');
                //row Akun
                $rowAkun = $this->kode_akun->rowById($kodeAkun);

                $keterangan = $this->input->post('keterangan');
                $jumlah = $this->input->post('jumlah');
                $harga = $this->input->post('harga');
                $this->beforeCreate($_POST);
                //create to database
                $insert = array();
                $insert['invoices_number'] = $this->mdl->createInvoiceNumber($kodeAkun);
                $insert['invoices_date'] = date('Y-m-d');
                $insert['kode_akun_id'] = $kodeAkun;
                $insert['keterangan'] = $keterangan;
                $insert['jumlah'] = $jumlah;
                $insert['harga'] = $harga;
                $insert['created_by'] = $this->session->userdata('user_id');
                $insert['created_on'] = date('Y-m-d H:i:s');
                $upload = uploadFile();
                if(!is_null($upload)) {
                    $insert["image"] = $upload;
                }else{
                    $insert["image"] = null;                    
                }

                if($rowAkun->saldo == 'debit'){
                    $insert['debit'] = $harga;
                    $insert['kredit'] = 0;
                }else{
                    $insert['debit'] = 0;
                    $insert['kredit'] = $harga;
                }
                $this->db->insert('cash_flow', $insert);


                $this->session->set_flashdata('success', self::SUCCESS_MESSAGE_SAVED);
                redirect($this->route);
			}else{
				$this->create();
			}
		}catch(Exception $e){
			var_dump($e);
		}
    }
    
    public function edit($id){
		checkPermission($this->route,"update");
		$this->load->model($this->model,"mdl");
		$data = $this->mdl->find($id);
        $items =[
            "data"=> $data,
            "akun" => $this->kode_akun->toSelectItem("akun", "id"),
        ];
		if(is_null($data)) show_error('Anda tidak diperkenankan mengakses halaman ini oleh administrator.', 403, 'Akses Ditolak'); 
		$this->template->content->view($this->route."/edit",$items);
        $this->template->publish();
	}

    protected function beforeCreate(array $data){ 
        $data["image"] = null;
        $upload = uploadFile();
        if(!is_null($upload)) $data["image"] = $upload;
        return $data; 
    }

    protected function beforeUpdate(array $data, $oldData, $id){ 
        if(!is_null($oldData->categories_room_image)){
            if(file_exists($oldData->categories_room_image)){
                unlink($oldData->categories_room_image);
            }
        }
        $data["image"] = null;
        $upload = uploadFile();
        if(!is_null($upload)) $data["image"] = $upload;
        return $data;
    }

    public function get_search(){
        $keyword = $this->input->post('search');
        $this->load->model($this->model, "mdl");
        $response = $this->mdl->get_search($keyword);
        echo json_encode($response);        
    }


}