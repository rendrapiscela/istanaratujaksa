<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends MY_Controller{

    public function __construct() {
        parent::__construct();
        $this->cleanInput = true;
        $this->route = "web/maintenance";
        $this->model = "Maintenance_model";
        $this->template->title = "Pemeliharaan Kamar";
        $this->template->javascript->add(site_url('assets/app/js/maintenance.js?v='.time()));
        $this->load->model($this->model, "mdl");
        $this->load->model("Status_room_model", "status");
		$this->load->model("Room_model", "room");
		$this->load->model("Setting_model", "setting");
		$this->load->model("Fasilitas_model", "fasilitas");
    }

    public function index(){
		checkPermission($this->route,"view");
        $data['data'] = $this->mdl->get_all();
		$this->template->content->view($this->route.'/index', $data);
        $this->template->publish();
	}

    public function pemeliharaan($idRoom){
		checkPermission($this->route,"create");
        //master room by id
        $data['room'] = $this->room->find($idRoom, 'id');
        $data['statusActivity'] = $this->setting->slug('status-activity')->config_value;
        $data['history'] = $this->mdl->historyMaintenance($idRoom);

		$this->template->content->view($this->route.'/pemeliharaan', $data);
        $this->template->publish();
    }

    public function tindak_lanjut($idRoom,$idTrouble){
		checkPermission($this->route,"create");
        //master room by id
        $data['room'] = $this->room->find($idRoom, 'id');
        $data['statusActivity'] = $this->setting->slug('status-activity')->config_value;
        $data['history'] = $this->mdl->historyMaintenance($idRoom);
        $data['trouble'] = $this->mdl->getTrouble($idTrouble);
        $data['statusRuangan'] = $this->status->getAll();    
        $data['idTrouble'] = $idTrouble;
		$this->template->content->view($this->route.'/tindak-lanjut', $data);
        $this->template->publish();
    }

    public function store_pemeliharaan($id){
        $this->form_validation->set_rules('tanggal_check', 'Tanggal Check', 'required');
        $this->form_validation->set_rules('status_activity', 'Status Activity', 'required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->route.'/pemeliharaan/'.$id);
        }else{
            $data = array(
                'room_id' => $id,
                'tanggal_check' => $this->input->post('tanggal_check'),
                'status_activity' => $this->input->post('status_activity'),
                'activity_name' => $this->input->post('activity_name'),
                'deskripsi' => $this->input->post('deskripsi'),
                'created_by' => $this->session->userdata('user_id'),
                'created_on' => date('Y-m-d H:i:s')
            );
            $this->mdl->insertData('room_trouble', $data);
            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect($this->route.'/pemeliharaan/'.$id);
        }
    }

    public function store_tindak_lanjut($id, $idTrouble){
        $this->form_validation->set_rules('solusi', 'Solusi', 'required');
        if ($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('error', validation_errors());
            redirect($this->route.'/pemeliharaan/'.$id);
        }else{
            //update status room
            $statusKamar = $this->input->post('status_kamar');
            $statusUpdate = array(
                'status_id' => $statusKamar
            );
            $this->mdl->updateData('rooms', $statusUpdate, $id);

            $data = array(
                'is_tl' => 'sudah-tl',
                'solution' => $this->input->post('solusi'),
                'budget' => $this->input->post('budget'),
                'updated_by' => $this->session->userdata('user_id'),
                'updated_on' => date('Y-m-d H:i:s')
            );
            $this->mdl->updateData('room_trouble', $data, $idTrouble);
            $this->session->set_flashdata('success', 'Pemasalahan sudah ditindaklanjuti !');
            redirect($this->route.'/pemeliharaan/'.$id);
        }
    }
    
    public function cek_rutin($idRoom){
		checkPermission($this->route,"create");
        //master room by id
        $data['room'] = $this->room->find($idRoom, 'id');
        $data['statusActivity'] = $this->setting->slug('status-activity')->config_value;
        $data['statusRutin'] = $this->setting->slug('status-rutin')->config_value;
        $history = $this->mdl->historyRutin($idRoom);
        foreach($history as $idx => $map){
            $detail = $this->mdl->historyRutinDetail($map->room_id, $map->created_at);
            //assign to object
            if(!empty($detail)){
                $history[$idx]->detail = $detail;
            }
        }
        $data['history'] = $history;
        $data['statusRuangan'] = $this->status->getAll();
        $data['fasilitas'] = $this->fasilitas->getAll();
		$this->template->content->view($this->route.'/cek_rutin', $data);
        $this->template->publish();
    }

    public function store_rutin($id){
        $tanggalCek = $this->input->post('tanggal_check');
        $arrayJenisCek = $this->input->post('jenis_check_id');
        $jumData = count($arrayJenisCek);
        $arrayKeterangan = $this->input->post('keterangan');
        $statusKamar = $this->input->post('status_kamar');
        //update status in room table
        $statusUpdate = array(
            'status_id' => $statusKamar
        );
        $this->mdl->updateData('rooms', $statusUpdate, $id);
        //looping data insert
        for($i=0; $i<$jumData; $i++){
            $statusCek = $this->input->post('status_check_id_'.($i+1));
            $data = array(
                'room_id' => $id,
                'created_at' => $tanggalCek,
                'status_id' => $statusKamar,
                'jenis_check_id' => $arrayJenisCek[$i],
                'kondisi' => $statusCek,
                'keterangan' => $arrayKeterangan[$i],
                'created_by' => $this->session->userdata('user_id')
            );
            $this->mdl->insertData('room_check', $data);
        }    

            $this->session->set_flashdata('success', 'Data berhasil disimpan');
            redirect($this->route);
    }

    protected function beforeCreate(array $data){ 
        $data["image"] = null;
        $upload = uploadFile();
        if(!is_null($upload)) $data["image"] = $upload;
        return $data; 
    }

    protected function beforeUpdate(array $data, $oldData, $id){ 
        if(!is_null($oldData->categories_room_image)){
            if(file_exists($oldData->categories_room_image)){
                unlink($oldData->categories_room_image);
            }
        }
        $data["image"] = null;
        $upload = uploadFile();
        if(!is_null($upload)) $data["image"] = $upload;
        return $data;
    }


}