<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Audit extends MY_Controller{

    public function __construct() {
        parent::__construct();
        $this->route = "web/audit";
        $this->model = "Audit_model";
        $this->template->title = "Audit Trail";
        $this->template->javascript->add(site_url('assets/app/js/audit.js'));
    }

    public function create(){
        show_404();
    }

    public function show($id){
        show_404();
    }

    public function edit($id){
        show_404();
    }

    public function delete($id){
        show_404();
    }

    public function penyesuaian_tarif(){
        //example path : http://localhost/hotelistanaratu/web/audit/penyesuaian_tarif/3?start_date=2022-12-20&end_date=2023-01-16
        //  186750 - 245000 | 186750
        // 220000 - 270000 | 207500
        // 240000 - 315000 | 275000
        if(!isset($_GET['start_date']) || !isset($_GET['end_date'])){
            echo "<b> Start date and end date is required </b>";
            die();
        }
        $tarifLama = [186750, 220000, 240000];
        $getTarif = $this->db->from('categories_room');
        $getTarif = $this->db->where('deleted_on', NULL);
        $daftarTarif = $getTarif->get()->result_array();

        $day = $this->uri->segment(4);
        //invoice transaction
        $where = "";
        if(!empty($day)){
            $where = " AND (SELECT count(invoice_room.room_id) FROM invoice_room WHERE invoice_room.invoice_id = invoices.id) = ".$day;
        }

        $queryInvoices = "SELECT * 
        FROM invoices
        WHERE invoices.check_in_on >= '".$_GET['start_date']."' AND invoices.check_in_on <= '".$_GET['end_date']."' AND invoices.id != 2507 AND tendered is not null
        $where
        ORDER BY invoices.check_in_on ASC
        ";
        $invoices = $this->db->query($queryInvoices);
        $invoices = $invoices->result();
        //print_r($daftarTarif);
        if(is_array($invoices) && count($invoices) > 0){
            $count = 1;
            foreach($invoices as $row){
                //get transaction room
                $transaction = $this->transactionRoomById($row->id);
                if(!empty($transaction)){
                    $total = 0;
                    foreach($transaction as $transRoom){
                        $idTarif = $transRoom->category_id;
                        //get new tarif from daftarTarif
                        $newTarif = 0;
                        foreach($daftarTarif as $tarif){
                            if($tarif['id'] == $idTarif){
                                //check payment_type
                                if($row->payment_type == 0){
                                    $newTarif = $tarif['cost'];
                                }
                                else{ //online 1
                                    $newTarif = $tarif['cost_online'];
                                }
                            }
                        } //end foreach daftarTarif
                    //update invoice_room with tarif
                    $updateInvoiceRoom = $this->db->where('invoice_id', $row->id);
                    $updateInvoiceRoom = $this->db->where('room_id', $transRoom->room_id);
                    $updateInvoiceRoom = $this->db->update('invoice_room', ['price' => $newTarif, 'total' => $newTarif * $transRoom->duration]);
                    //log
                    $total += $newTarif * $transRoom->duration;
                    echo $row->id." - ".$transRoom->room_id." | Tarif Lama ".$transRoom->price." - Tarif Baru ".$newTarif." | room id ".$transRoom->category_id." | Bayar ".$row->payment_type." | Total ".$total." | Durasi ".$transRoom->duration." | ".$row->check_in_on." | ".$row->check_out_on;
                    echo "<br/>";
                    }
                }
                // die();
                //update due & tendered invoice
                $updateInvoice = $this->db->where('id', $row->id);
                if($row->payment_type == 0){
                    $updateInvoice = $this->db->update('invoices', ['due' => $total, 'tendered' => $total]);
                }
                else{ //online 1
                    $updateInvoice = $this->db->update('invoices', ['due' => $total]);
                }

                echo $count.'. ' .$row->id." - Total Bayar Lama : ".$row->due." - Total Bayar Baru : ".$total." END ";
                echo "<hr/>";

                $count++;
            }//end foreach invoices

        }//end if
    }

    function transactionRoomById($id){
        //get transaction room
        $getTransaction = $this->db->select('invoice_room.*, rooms.number as room_name, rooms.category_id as category_id');
        $getTransaction = $this->db->from('invoice_room');
        $getTransaction = $this->db->join('rooms', 'rooms.id = invoice_room.room_id');
        $getTransaction = $this->db->where('invoice_id', $id);
        $getTransaction = $this->db->get();
        $transaction = $getTransaction->result();
        return $transaction;
    }

}