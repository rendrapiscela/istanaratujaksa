<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Room_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'rooms';
    }

    public function dataTableColumnFilter(){
        return [
            "rooms.id",
            "rooms.number",
            "categories_room.name",
            "status_room.name",
            "rooms.capacity",
            "rooms.occupant",
            "rooms.created_on",  
        ];
    }

    public function createValidation($form) {
        $form->set_rules('number', 'Nomor Kamar', 'required|is_unique[' . $this->table . '.number]');
        $form->set_rules('category_id', 'Kategori Kamar', 'required');
        $form->set_rules('status_id', 'Status Kamar', 'required');
        $form->set_rules('capacity', 'Kapasitas', 'required');
        $form->set_rules('occupant', 'Jumlah Penghuni', 'required');
    }

    public function updateValidation($form, $id) {
        $form->set_rules('number', 'Nomor Kamar', 'required|edit_unique['.$this->table.'.number.' . $id . ']');
        $form->set_rules('category_id', 'Kategori Kamar', 'required');
        $form->set_rules('status_id', 'Status Kamar', 'required');
        $form->set_rules('capacity', 'Kapasitas', 'required');
        $form->set_rules('occupant', 'Jumlah Penghuni', 'required');
    }

    public function get_category($id){
        $this->db->where("category_id", $id);
        $this->db->where($this->table.".deleted_on", null);
        $this->db->where($this->table.".deleted_by", null);
        $this->db->where($this->table.".occupant", 0);
        $this->db->order_by("number", "ASC");
        return $this->db->get($this->table)->result();
    }

    protected function belongsTo(){ 
        return array(
            [
                "target"=>"categories_room",
                "foreign_key"=>"category_id"
            ],
            [
                "target"=>"status_room",
                "foreign_key"=>"status_id"
            ]
        );
    }

    public function rowRoom($id){
        $this->db->select("rooms.*, categories_room.name as category_name, categories_room.cost as price, categories_room.description as category_description, categories_room.image as category_image");
        $this->db->from($this->table);
        $this->db->where("rooms.id", $id);
        $this->db->join("categories_room", "categories_room.id = rooms.category_id");
        return $this->db->get()->row();
    }

    public function infoRooms($status = null){ //status kosong or isi
        //query update
        $cleaningRoom = "
        UPDATE rooms SET occupant = 0
        WHERE id NOT IN (
            SELECT invoice_room.room_id
            FROM invoice_room
            WHERE invoice_id IN (
            SELECT id
            FROM invoices
            WHERE check_out_on is null AND deleted_on is null
            )
        )
        ";
        $clean = $this->db->query($cleaningRoom);

        $where = '';
        if($status == 'kosong'){
            $where = 'WHERE rooms.occupant = 0';
        }elseif($status == 'isi'){
            $where = 'WHERE rooms.occupant > 0';
        }
        $sql = "
        SELECT COUNT(rooms.category_id) as jumlah_kamar,
            categories_room.`name` as nama,
            GROUP_CONCAT(rooms.number) as nama_room
        FROM rooms
        JOIN categories_room ON categories_room.id = rooms.category_id
        $where
        GROUP BY rooms.category_id
        ";
        $data = $this->db->query($sql);
        return $data->result_array();
    }

    public function statusRoom(){
        $sql = "SELECT categories_room.id as kategori_kamar_id, 
        categories_room.`name` as nama_kategori,
        rooms.id as room_id,rooms.number as nomor_kamar, 
        status_room.id as status_id,
        status_room.`name` as status_kamar, 
        status_room.description as keterangan_kamar,
        SUBSTRING(rooms.number, 1,1) as lantai_kamar
        FROM rooms
        JOIN categories_room ON categories_room.id = rooms.category_id
        JOIN status_room ON status_room.id = rooms.status_id
        ORDER BY rooms.number ASC";
        
        $data = $this->db->query($sql);
        return $data->result_array();
    }

}