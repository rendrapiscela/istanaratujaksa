<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Fasilitas_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'jenis_check';
    }

    public function dataTableColumnFilter(){
        return [
            "jenis_check.id",
            "jenis_check.nama_check",
            "jenis_check.keterangan",
            "jenis_check.merk",
            "jenis_check.created_on",  
            "jenis_check.created_by"
        ];
    }

    public function createValidation($form) {
        $form->set_rules('nama_check', 'Nama Fasilitas', 'required|is_unique[' . $this->table . '.nama_check]');
    }

    public function updateValidation($form, $id) {
        $form->set_rules('nama_check', 'Nama Fasilitas', 'required|edit_unique['.$this->table.'.nama_check.' . $id . ']');
    }

    public function getRow($id){
        return $this->db->where("id", $id)->get($this->table)->row();
    }

}