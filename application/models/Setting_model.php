<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Setting_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'settings';
    }

    public function slug($slug){
        $this->db->where("slug", $slug);
        return $this->db->get($this->table)->row();
    }

}