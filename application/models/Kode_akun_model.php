<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kode_akun_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'kode_akun';
    }

    public function getAll(){
        $this->db->where($this->table.".deleted_on", null);
        $this->db->where($this->table.".deleted_by", null);
        return $this->db->get($this->table)->result();
    }

    public function rowById($id){
        $this->db->where($this->table.".id", $id);
        return $this->db->get($this->table)->row();
    }

}