<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenance_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table_room = 'rooms';
        $this->room_trouble = 'room_trouble';
        $this->room_check = 'room_check';
    }

    public function get_all($limit = null, $offset = null, $search = null, $sort = null, $order = null){
        $this->db->select("rooms.*,
            (SELECT status_room.name FROM status_room WHERE status_room.id = rooms.status_id) AS status,
            (SELECT categories_room.name FROM categories_room WHERE categories_room.id = rooms.category_id) AS categories_room_name,
            (SELECT count(DISTINCT room_check.created_at) FROM room_check WHERE room_check.room_id = rooms.id) AS jumlah_cek,
            (SELECT SUM(room_trouble.budget) FROM room_trouble WHERE room_trouble.room_id = rooms.id) AS budget_trouble,
            (SELECT room_check.created_at FROM room_check WHERE room_check.room_id = rooms.id ORDER BY room_check.created_at DESC LIMIT 1) AS tanggal_cek
            "
        );
        $this->db->limit($limit, $offset);
        $this->db->from($this->table_room);
        if(!is_null($search)){
            $this->db->like('rooms.number', $search);
        }
        if(!is_null($sort) && !is_null($order)){
            $this->db->order_by($sort, $order);
        }
        $this->db->where($this->table_room.".deleted_on", null);
        $this->db->where($this->table_room.".deleted_by", null);

        $query = $this->db->get();
        return $query->result();
    }

    public function historyMaintenance($roomId){
        $this->db->select("room_trouble.*,
            (SELECT users_profile.first_name FROM users_profile WHERE users_profile.user_id = room_trouble.created_by) AS user_name");
        $this->db->where('room_id', $roomId);
        $this->db->where('deleted_on', null);
        $this->db->where('deleted_by', null);
        $this->db->from($this->room_trouble);
        $this->db->order_by('is_tl', 'ASC');
        $query = $this->db->get();
        return $query->result();
    }

    public function historyRutin($roomId){
        $this->db->select("room_check.*,
            (SELECT users_profile.first_name FROM users_profile WHERE users_profile.user_id = room_check.created_by) AS user_name,
            (SELECT status_room.name FROM status_room WHERE status_room.id = room_check.status_id) AS status_name"
        );
        $this->db->where('room_check.room_id', $roomId);
        $this->db->from($this->room_check);
        $this->db->group_by(['room_check.created_at','room_check.room_id']);
        $this->db->order_by('room_check.created_at', 'DESC');
        $query = $this->db->get();
        return $query->result();
    }

    public function historyRutinDetail($roomId, $createdAt){
        $this->db->select("room_check.*,
            (SELECT users_profile.first_name FROM users_profile WHERE users_profile.user_id = room_check.created_by) AS user_name,
            (SELECT jenis_check.nama_check FROM jenis_check WHERE jenis_check.id = room_check.jenis_check_id) AS nama_check
            "
        );
        $this->db->where('room_id', $roomId);
        $this->db->where('created_at', $createdAt);
        $this->db->from($this->room_check);
        $query = $this->db->get();
        return $query->result();
    }

    public function getTrouble($id){
        $this->db->select("room_trouble.*,
            (SELECT users_profile.first_name FROM users_profile WHERE users_profile.user_id = room_trouble.created_by) AS user_name");
        $this->db->where('id', $id);
        $this->db->from($this->room_trouble);
        $query = $this->db->get();
        return $query->row();
    }

    public function insertData($table, $data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }
    public function updateData($table, $data, $id){
        $this->db->where('id', $id);
        return $this->db->update($table, $data);
    }

}