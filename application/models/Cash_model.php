<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cash_model extends MY_Model{

    public function __construct(){
        parent::__construct();
        $this->table = 'cash_flow';
    }

    public function dataTableColumnFilter(){
        return [
            "cash_flow.id",
            "cash_flow.invoices_number",
            "cash_flow.invoices_date",
            "cash_flow.keterangan",
            "cash_flow.debit",
            "cash_flow.kredit",
            "cash_flow.kode_akun_id",
            "cash_flow.created_on",
        ];
    }

    public function createValidation($form) {
        $form->set_rules('kode_akun_id', 'Akun', 'required');
        $form->set_rules('jumlah', 'Jumlah/Unit', 'required');
        $form->set_rules('harga', 'Harga', 'required');
    }

    public function updateValidation($form, $id) {
        $form->set_rules('kode_akun_id', 'Akun', 'required');
        $form->set_rules('jumlah', 'Jumlah/Unit', 'required');
        $form->set_rules('harga', 'Harga', 'required');
    }

    public function get_category(){
        $this->db->where($this->table.".deleted_on", null);
        $this->db->where($this->table.".deleted_by", null);
        return $this->db->get($this->table)->result();
    }

    protected function belongsTo(){ 
        return array(
            [
                "target"=>"users",
                "foreign_key"=>"created_by"
            ],
            [
                "target"=>"kode_akun",
                "foreign_key"=>"kode_akun_id"
            ],
        );
    }

    public function createInvoiceNumber($kodeAkun)
    {
        $now = date("Y-m-d");
        $this->db->select_max('invoices_number');
        $this->db->where("invoices_date", $now);
        $result = $this->db->get("cash_flow")->row();
        if (!is_null($result->invoices_number)) {
            $number = explode("/", $result->invoices_number);
            $counter = index_number((int)end($number) + 1, 5);
            return "KAS/" . date('Ymd') . "/" . $counter;
        } else {
            return 'KAS/'. date('Ymd') . '/00001';
        }
    }

    public function get_search($keyword){
        $this->db->select("keterangan as id, keterangan as name");
        $this->db->where("deleted_on", null);
        $this->db->where("deleted_by", null);
        $this->db->like("LOWER(keterangan)", strtolower($keyword));
        $this->db->limit(10);
        return $this->db->get($this->table)->result_array();
    }

}