<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Versi</b> 1.0.0
    </div>
    <strong>Copyright &copy; <?php echo date("Y");?> <a href="https://hotelistanaratu.com">Istana Ratu Hotel</a>.</strong> All
    rights reserved.
</footer>