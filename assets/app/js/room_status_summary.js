//document ready
$(document).ready(function () {
    //sidebar toggle click if screen size is less than 768px
    if ($(window).width() > 568) {
        $('.sidebar-toggle').click();
    }
    $(".button-room").on('click', function(){
        //get data-id attribute of the clicked element
        var idRoom = $(this).data('id');
        //get status-id attribute of the clicked element
        var statusRoom = $(this).data('status');
        if(statusRoom === 4){
            $("#modal-check-in").modal('show');
        }
        //send ajax
        $.ajax({
            url: BASE_URL+'web/reservation/set_session_room',
            type: 'POST',
            data: {idRoom: idRoom, csrf_test_name: CSRF_VALUE},
            dataType: 'json',
            success: function(data){

            }
        });
    })
});