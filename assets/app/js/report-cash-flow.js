$(document).ready(function(){

    var getType = function(){
        var type = "";
        $(".tab-pane").each(function(){
            if($(this).hasClass("active")){
                type = $(this).attr("id");
            }
        });
        return type;
    };


    $("#btn-filter-cash").click(function(e){
        e.preventDefault();
        $("#modal-filter-cash").modal("show"); 
        return false; 
    });

    $("#btn-print-cash").click(function(e){
        e.preventDefault();
        let type = getType();
        if(type === "tab_1_cash"){
            let url = $("#report-cash-flow").attr("src");
            let W = window.open(url);
            W.window.print();
        }
        return false;
    });
    
    $("#filter-data-cash").submit(function(e){
        e.preventDefault();
        var first = $("#first").val();
        var last = $("#last").val();
        let type = getType();

        if(!first || !last){
            swal("Tanggal tidak lengkap", "Tanggal Awal atau Tanggal Akhir Kosong !!", "error");
        }else{
            if(type === "tab_1_cash"){
                let param = first+"_"+last;
                let url = BASE_URL+"web/report/report_cash_flow/"+param;
                $("#report-cash-flow").attr("src", url);
                $("#modal-filter-cash").modal("hide"); 
            }
        }

        return false;
    });

   

});