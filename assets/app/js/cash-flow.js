$(document).ready(function() {

    var mainArea = "web";
    var mainRoute = "cash_flow";
    var renderConfig = {
        "table": "#table",
        "route": mainRoute,
        "request": "POST",
        "area": mainArea,
        "column": [{
                "targets": 0,
                "orderable": false,
                "className": "text-center",
                "render": function(data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }
            },
            {
                "targets": 1,
                "data": "cash_flow_invoices_number"
            },
            {
                "targets": 2,
                "data": "cash_flow_invoices_date"
            },
            {
                "targets": 3,
                "data": "kode_akun_akun"
            },
            {
                "targets": 4,
                "data": "cash_flow_keterangan"
            },
            {
                "targets": 5,
                "data": "cash_flow_harga",
                "className": "text-right",
                "render": function(data, type, row, meta) {
                    return formatRupiah(data, 'Rp. ');
                }
            },
            {
                "targets": 6,
                "orderable": false,
                "className": "text-center",
                "render": function(data, type, row, meta) {
                    var config = {
                        "route": mainRoute,
                        "id": row.cash_flow_id,
                        "area": "web"
                    };
                    return appDataTable.action(config);
                }
            },
        ]
    };

    appDataTable.render(renderConfig);

    //set autocomplete search
    $("#keterangan").keyup(function(){
        var search = $(this).val();
        if(search == ""){
            $("#searchResult").empty();
            return;
        }
        //get base url
        var url = BASE_URL + mainArea + "/" + mainRoute + "/get_search";
        if(search != "" && search.length > 2){
            $.ajax({
                url: url,
                type: 'post',
                data: {search:search, csrf_test_name: CSRF_VALUE},
                dataType: 'json',
                success:function(response){
                
                    var len = response.length;
                    $("#searchResult").empty();
                    for( var i = 0; i<len; i++){
                        var id = response[i]['id'];
                        var name = response[i]['name'];

                        $("#searchResult").append("<li value='"+id+"'>"+name+"</li>");

                    }

                    // binding click event to li
                    $("#searchResult li").bind("click",function(){
                        setText(this);
                    });
                }
            });
        }
    });
});

// Set Text to search box and get details
function setText(element){
    var value = $(element).text();
    $("#keterangan").val(value);
    $("#searchResult").empty();    
}